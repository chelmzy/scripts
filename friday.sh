#Plays Rebecca Black's Friday every friday at 7AM on Roku
# Make sure to set HOST
# Crontab should be 0 7 * * 5 /usr/local/bin/friday.sh >/dev/null 2>&1
HOST=192.168.2.83
PORT=8060
ping -c1 $HOST 1>/dev/null 2>/dev/null
SUCCESS=$?
if [ $SUCCESS -eq 0 ]
then
  curl -d '' http://$HOST:$PORT/keypress/Home
  sleep 1
  curl -d '' http://$HOST:$PORT/launch/837?contentID=iCFOcqsnc9Y
else
  curl -d '' http://$HOST:$PORT/keypress/Power
  sleep 1
  curl -d '' http://$HOST:$PORT/launch/837?contentID=iCFOcqsnc9Y
fi
#EOF
