package main
//Grabs domains from top cryptocurrency using Blockmodo json dump
//Use with all.json

import (
	"encoding/json"
	"fmt"
	test "golang.org/x/net/publicsuffix"
	"io/ioutil"
	"net/url"
	"os"
)

type Coins []struct {
	Website string `json:"website"`
}

func main() {

	// Open our jsonFile
	jsonFile, err := os.Open("all.json")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully opened all.json")
	// defer the closing of our jsonFile so that we can parse it later on
	defer jsonFile.Close()

	byteValue, _ := ioutil.ReadAll(jsonFile)

	var coins Coins
	json.Unmarshal(byteValue, &coins)

	for _, i := range coins {
		if i.Website != "-" {
			websiteFull, err := url.Parse(i.Website)
			
			if err != nil {
				continue
			}

			websiteDomain, err := test.EffectiveTLDPlusOne(websiteFull.Host)
			if err != nil {
				continue
			}

			fmt.Println(websiteDomain)
		}

	}

}
